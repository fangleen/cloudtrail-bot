terraform {
    backend "s3" {
        bucket  = "fangleen-cloudtrail-to-s3"
        key     = "cloudtrail_bot/terraform.tfstate"
        region  = "us-west-2"
        encrypt = "true"
        profile = "default"
    }
}
